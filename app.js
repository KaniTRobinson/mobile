import React, { Component } from 'react';
import { AsyncStorage, View, Text, StatusBar } from 'react-native';
import { Actions, Scene, Router } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';

import HomePage from './templates/HomePage';
import SearchPage from './templates/SearchPage';
import ProfilePage from './templates/ProfilePage';
import LoginPage from './templates/LoginPage';
import BrowsePage from './templates/BrowsePage';
import RegisterPage from './templates/RegisterPage';
import PlaylistPage from './templates/PlaylistPage';
import SettingsPage from './templates/SettingsPage';
import GenrePlaylists from './templates/GenrePlaylists';

import application from './styles/application';

import UserActions from './actions/user';
import UserStore from './stores/user';

import GenreActions from './actions/genre';
import GenreStore from './stores/genre';

const TabIcon = ({ selected, title }) => {
    var icon = null,
        tabTitle = '';

    switch(title) {
        case 'MUSIC':
            icon = 'star-o';
            tabTitle = 'Featured';
            break;
        case 'BROWSING':
            icon = 'folder-open-o';
            tabTitle = 'Browse';
            break;
        case 'SEARCHING':
            icon = 'search';
            tabTitle = 'Search';
            break;
        case 'USER':
            icon = 'user';
            tabTitle = 'Account';
            break;
        default:
            icon = 'ellipsis-v';
            tabTitle = '';
            break;
    }

    return (
        <View>
            <StatusBar backgroundColor="blue" barStyle="light-content" />
            <Icon name={ icon } style={[ application.tabBarIcon, selected && application.coloredText ]} size={ 20 }/>
            <Text style={[ application.tabBarText, selected && application.coloredText ]}>{ tabTitle }</Text>
        </View>
    );
};

const scenes = Actions.create(
    <Scene key="main">
        <Scene key='tabBar' tabs tabBarStyle={ application.tabBar }>
            {/* Music Routes */}
            <Scene
                key="music"
                title="MUSIC"
                icon={ TabIcon }
                navigationBarStyle={ application.navBar }
                titleStyle={ application.navBarTitle }>
                <Scene
                    key="home"
                    title="FEATURED"
                    renderBackButton={ () => (null) }
                    panHandlers={ null }
                    duration={ 0 }
                    component={ HomePage }
                    initial/>
                <Scene
                    key="playlist"
                    title="UNTITLED PLAYLIST"
                    leftButtonIconStyle={{ tintColor: '#FFF' }}
                    duration={0}
                    component={ PlaylistPage }/>
            </Scene>
            {/* Browsing Routes */}
            <Scene
                key="browsing"
                title="BROWSING"
                icon={ TabIcon }
                navigationBarStyle={ application.navBar }
                titleStyle={ application.navBarTitle }>
                <Scene
                    key="browse"
                    title="BROWSE"
                    renderBackButton={ () => (null) }
                    panHandlers={ null }
                    duration={0}
                    component={ BrowsePage } />
                <Scene
                    key="genrePlaylists"
                    title="UNTITLED GENRE"
                    leftButtonIconStyle={{ tintColor: '#FFF' }}
                    duration={0}
                    component={ GenrePlaylists } />
                <Scene
                    key="genrePlaylist"
                    title="UNTITLED PLAYLIST"
                    leftButtonIconStyle={{ tintColor: '#FFF' }}
                    duration={0}
                    component={ PlaylistPage }/>
            </Scene>
            {/* Searching Routes */}
            <Scene
                key="searching"
                title="SEARCHING"
                icon={ TabIcon }
                navigationBarStyle={ application.navBar }
                titleStyle={ application.navBarTitle }>
            <Scene
                key="search"
                title="SEARCH"
                renderBackButton={ () => (null) }
                panHandlers={ null }
                duration={0}
                component={ SearchPage } />
            </Scene>
            {/* User Routes */}
            <Scene
                key="user"
                title="USER"
                icon={ TabIcon }
                navigationBarStyle={ application.navBar }
                titleStyle={ application.navBarTitle }>
                <Scene
                    key="login"
                    title="LOGIN"
                    nonAuthOnly={ true }
                    renderBackButton={ () => (null) }
                    panHandlers={ null }
                    duration={0}
                    component={ LoginPage } />
                <Scene
                    key="profile"
                    title="PROFILE"
                    authOnly={ true }
                    renderBackButton={ () => (null) }
                    panHandlers={ null }
                    duration={0}
                    component={ ProfilePage } />
                <Scene
                    key="settings"
                    title="SETTINGS"
                    authOnly={ true }
                    leftButtonIconStyle={{ tintColor: '#FFF' }}
                    duration={0}
                    component={ SettingsPage } />
                <Scene
                    key="myPlaylist"
                    title="UNTITLED PLAYLIST"
                    leftButtonIconStyle={{ tintColor: '#FFF' }}
                    duration={0}
                    component={ PlaylistPage }/>
                <Scene
                    key="register"
                    title="REGISTER"
                    nonAuthOnly={ true }
                    leftButtonIconStyle={{ tintColor: '#FFF' }}
                    duration={0}
                    component={ RegisterPage } />
            </Scene>
        </Scene>
    </Scene>
);

export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = Object.assign(UserStore.getState(), GenreStore.getState());
        this.onUserChange = this.onUserChange.bind(this);
        this.onGenresChange = this.onGenresChange.bind(this);
    }

    componentWillMount() {
        UserStore.listen(this.onUserChange);
        GenreStore.listen(this.onGenresChange);
        UserActions.getMe();
        GenreActions.get();
    }

    onUserChange(state) {
        this.setState({
            user: state.user,
            token: state.token
        });
    }

    onGenresChange(state) {
        this.setState({
            genres: state.genres
        });
    }

    render() {
        return (
            <Router isLoggedIn={ (this.state.user !== null) } user={ this.state.user } token={ this.state.token } genres={ this.state.genres } scenes={ scenes }/>
        );
    }
}
