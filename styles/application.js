import React, { Component } from 'react';
import { StyleSheet } from 'react-native';

const constants = {
    container: {
        top: 64,
        bottom: 50
    },
    backgroundColor: '#1F1F1F',
    navBackgroundColor: '#111',
    textColor: '#FFF',
    lightTextColor: '#CCC',
    borderColor: 'rgba(255, 255, 255, 0.05)',
    borderColor2: '#CCC',
    brandingColor: '#109C9D',
    redColor: '#d44950'
};

const application = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: constants.container.top,
        marginBottom: constants.container.bottom,
        flexDirection: 'column',
        backgroundColor: constants.backgroundColor
    },
    navBar: {
        backgroundColor: constants.navBackgroundColor,
        borderBottomWidth: 0
    },
    navBarTitle: {
        color: constants.textColor,
        fontSize: 14
    },
    flex: {
        flex: 1
    },

    pushTop: {
        marginTop: 10
    },

    pushRight: {
        marginRight: 10
    },

    alignContents: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    flexRow: {
        flexDirection: 'row'
    },
    tabBar: {
        backgroundColor: constants.navBackgroundColor,
        borderTopWidth: 1,
        borderColor: constants.borderColor
    },
    tabBarText: {
        fontSize: 10,
        marginTop: 3,
        color: constants.lightTextColor
    },
    tabBarIcon: {
        textAlign: 'center',
        color: constants.lightTextColor
    },
    hidden: {
        opacity: 0
    },

    // Button
    buttonActive: {
      borderColor: constants.brandingColor
    },

    buttonFill: {
        backgroundColor: constants.brandingColor,
        borderColor: constants.brandingColor
    },

    button: {
        paddingHorizontal: 20,
        paddingVertical: 10,
        borderWidth: 1,
        borderColor: constants.borderColor2,
        borderRadius: 20
    },

    buttonRed: {
        borderColor: constants.redColor,
        backgroundColor: constants.redColor
    },

    buttonText: {
        color: constants.textColor,
        fontSize: 14,
        textAlign: 'center'
    },

    buttonSmallText: {
        color: constants.textColor,
        fontSize: 10,
        textAlign: 'center'
    },

    input: {
      backgroundColor: '#FFF',
      width: '90%',
      height: 40,
      borderRadius: 20,
      textAlign: 'center',
      marginBottom: 15,
      alignSelf: 'center'
    },

    inputTextSmaller: {
        fontSize: 14
    },

    center90: {
      width: '90%',
      alignSelf: 'center'
    },

    alignCenter: {
      alignSelf: 'center'
    },

    centerText: {
      textAlign: 'center'
    },

    alignChildren: {
        alignItems: 'center',
        justifyContent: 'center'
    },


    // Listview Styling
    list: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        margin: 5
    },

    removeListMargin: {
      margin: 0
    },

    item: {
        margin: 5,
        width: "46%"
    },

    // Title Styling
    title: {
      width: "100%",
      marginHorizontal: 5,
      marginTop: 10,
      marginBottom: 0
    },
    titleText: {
      color: constants.borderColor2,
      fontSize: 18
    },
    text: {
      color: constants.textColor
    },

    lightText: {
        color: constants.lightTextColor
    },

    coloredText: {
        color: constants.brandingColor
    },
    pageLogo: {
      width: 150,
      height: 150,
      resizeMode: 'stretch'
    },

    // Playlist List Styling
    playListImage: {
      width: '100%',
      paddingBottom: '100%',
      position: 'relative'
    },
    playListTitle: {
      color: constants.textColor,
      fontWeight: 'bold',
      fontSize: 12,
      marginTop: 5
    },
    playListAuthor: {
      fontSize: 10,
      marginTop: 5,
      color: constants.borderColor2
    },

    header: {
        width: '100%',
        height: 250,
    },

    HeaderImageContainer: {
      width: '100%',
      height: 250,
      justifyContent: 'center',
      alignItems: 'center'
    },

    HeaderImageContainerBlend: {
      backgroundColor: constants.navBackgroundColor
    },

    playListHeaderImage: {
        width: 130,
        height: 130
    },

    headerText: {
      color: constants.textColor,
      fontSize: 20,
      fontWeight: 'bold',
      marginTop: 10
    },

    headerAuthor: {
      color: constants.borderColor2,
      fontSize: 10,
      marginBottom: 10
    },
    songItem: {
      paddingVertical: 10,
      borderTopWidth: 1,
      borderBottomWidth: 1,
      borderColor: constants.borderColor
    },
    songIDContainer: {
      width: '10%',
      marginRight: 10
    },
    songID: {
      textAlign: 'right'
    },
    song: {
      width: '90%'
    },
    songName: {
      fontSize: 12,
      marginBottom: 3
    },
    songAuthor: {
      fontSize: 10
    },
    genreContainer: {
      width: '100%',
      height: 150,
      shadowOffset:{  width: 5,  height: 5,  }
    },
    genreImage: {
      width: "100%",
    	height: 150,
    	opacity: .8
    },
    genreText: {
      textShadowOffset: {
    		width: 2,
    		height: 1
    	},
    	textShadowRadius: 1,
    	textShadowColor: '#000',
    	height: 22,
    	backgroundColor: 'transparent',
    	color: '#FFF',
    	marginTop: 120,
    	marginLeft: 10,
    	fontSize: 16
    },
    headerPush: {
      marginBottom: 90
    },
    headerAvatar: {
      width: 100,
      height: 100,
      borderRadius: 50,
      borderWidth: 3,
      borderColor: '#FFF'
    },
    statsBar: {
      backgroundColor: 'rgba(0, 0, 0, 0.3)',
      paddingVertical: 10
    },
    statsText: {
      fontSize: 12
    },
    musicPlayerContainer: {
      width: '100%',
      position: 'absolute',
      bottom: 0,
      padding: 10,
      backgroundColor: 'rgba(0, 0, 0, .9)'
    }
});

export default application;
