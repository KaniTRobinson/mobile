import alt from '../alt';
import PlaylistsActions from '../actions/playlists'

export class PlaylistsStore {
  constructor() {
    this.userPlaylists = [];
    this.playlists = [];
    this.genrePlaylists = [];
    this.playlist = {};

    this.bindListeners({
      handleUpdatePlaylists: PlaylistsActions.UPDATE_PLAYLISTS,
      handleGetPlaylists: PlaylistsActions.GET_PLAYLISTS,
      handleUpdatePlaylist: PlaylistsActions.UPDATE_PLAYLIST,
      handleGetPlaylist: PlaylistsActions.GET_PLAYLIST,
      handleGetPlaylist: PlaylistsActions.GET_PLAYLIST,
      handleUpdateLikePlaylist: PlaylistsActions.UPDATE_LIKE_PLAYLIST,
      handleUpdatePlaylistById: PlaylistsActions.UPDATE_PLAYLIST_BY_ID,
      handleUpdateGetGenrePlaylists: PlaylistsActions.UPDATE_GET_GENRE_PLAYLISTS
    });
  }

  handleUpdateGetGenrePlaylists(genrePlaylists) {
      this.genrePlaylists = genrePlaylists;
  }

  handleUpdatePlaylistById(userPlaylists) {
      this.userPlaylists = userPlaylists;
  }

  handleUpdateLikePlaylist(response) {
      this.playlist.hasLiked = response.hasLiked;
      this.playlist.like_id = response.like_id;
  }

  handleUpdatePlaylist(playlist) {
      this.playlist = playlist;
  }

  handleGetPlaylist() {}

  handleUpdatePlaylists(playlists) {
      this.playlists = playlists;
  }

  handleGetPlaylists() {}
}


export default alt.createStore(PlaylistsStore, 'PlaylistsStore');
