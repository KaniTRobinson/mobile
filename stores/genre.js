import alt from '../alt';
import GenreActions from '../actions/genre'

export class GenreStore {
  constructor() {
    this.genres = [];

    this.bindListeners({
      handleUpdateGenres: GenreActions.UPDATE_GENRES,
      handleGet: GenreActions.GET
    });
  }

  handleUpdateGenres(genres) {
      this.genres = genres;
  }

  handleGet() {}
}


export default alt.createStore(GenreStore, 'GenreStore');
