import alt from '../alt';
import SongsActions from '../actions/songs'

export class SongsStore {
  constructor() {
    this.songs = [];
    this.isFollowing = false;

    this.bindListeners({
      handleUpdateSongs: SongsActions.UPDATE_SONGS,
      handleGetSongs: SongsActions.GET_SONGS,
    });
  }

  handleUpdateSongs(songs) {
      this.songs = songs;
  }

  handleGetSongs() {}
}


export default alt.createStore(SongsStore, 'SongsStore');
