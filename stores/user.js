import alt from '../alt';
import UserActions from '../actions/user'

export class UserStore {
    constructor() {
        this.me = {};
        this.token = '';
        this.user = {};

        this.bindListeners({
            handleUpdateSessions: UserActions.UPDATE_SESSIONS,
            handleUpdateGet: UserActions.UPDATE_GET,
            handleUpdateGetUserById: UserActions.UPDATE_GET_USER_BY_ID,
            handleUpdateUploadProfilePicture: UserActions.UPDATE_UPLOAD_PROFILE_PICTURE
        });
    }

    handleUpdateUploadProfilePicture(data) {
        let userData = this.user,
            meData = this.me;

        userData['image'] = data.original;
        meData['image'] = data.original;

        this.user = userData;
        this.me = meData;
    }

    handleUpdateSessions(user) {
        this.token = user.token;
        this.user = user.user;
        this.username = '';
        this.first_name = '';
        this.last_name = '';
        this.email = '';
        this.password = '';
        this.password_confirmation = '';
    }

    handleUpdateGet(user) {
        this.me = user.user;
        this.token = user.token;
    }

    handleUpdateGetUserById(user) {
        this.user = user;
    }
}


export default alt.createStore(UserStore, 'UserStore');
