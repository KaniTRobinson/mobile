import { AsyncStorage, Alert } from 'react-native';
import alt from '../alt';
import network from '../lib/networking';
import { Actions } from 'react-native-router-flux';

export class UserActions {

    logout() {
        var self = this;

        return (dispatch) => {
            network.request('logout', 'POST', [])
            .then(() => {
                AsyncStorage.removeItem('token');
                AsyncStorage.removeItem('user');
                self.updateGet({
                  'user': {},
                  'token': ''
                });
                Actions.login();
            });
        };
    }

    setSessions(data) {
        var self = this;

        return (dispatch) => {
            dispatch();

            if(!data.error) {
                self.updateSessions(data);
                AsyncStorage.setItem('token', data.token);
                AsyncStorage.setItem('user', JSON.stringify(data.user));
                Actions.profile();
            } else {
                console.log(data);
            }
        };
    }

    updateSessions(data) {
        return data;
    }

    register(request) {
        var self = this;

        return (dispatch) => {
            dispatch();
            network.request('auth/create', 'POST', [], request)
            .then((data) => {
                self.setSessions(data);
            })
            .catch((error) => {
                Alert.alert(
                  'Register Error',
                  error.error[0][Object.keys(error.error[0])[0]][0]
                );
            });
        };
    }

    login(request) {
        var self = this;

        return (dispatch) => {
            dispatch();
            network.request('authenticate', 'POST', [], request)
            .then((data) => {
                self.setSessions(data);
            })
            .catch((error) => {
                Alert.alert(
                  'Login Error',
                  error.error
                );
            });
        };
    }

    updateGet(data) {
        return data;
    }

    getMe() {
        var self = this,
            data = {};

        return (dispatch) => {
            dispatch();
            AsyncStorage.getItem('token', (tokErr, token) => {
                if(token) {
                    data['token'] = token;
                    AsyncStorage.getItem('user', (userErr, user) => {
                        if(user) {
                            data['user'] = (typeof user === 'object') ? user : JSON.parse(user);
                            self.updateGet(data);
                        }
                        network.request('user', 'GET')
                        .then((userQuery) => {
                            if(userQuery.id) {
                                data['user'] = userQuery;
                                AsyncStorage.setItem('user', JSON.stringify(userQuery));
                                self.updateGet(data);
                            }
                        })
                        .catch((error) => {
                          alert(JSON.stringify(error));
                        });
                    });
                }
            });
        };
    }

    updateGetUserById(user) {
        return user;
    }

    getUserById(id) {
      var self = this;

      return (dispatch) => {
          dispatch();
          network.request('user/' + id, 'GET')
          .then((user) => {
              self.updateGetUserById(user);
          })
          .catch((error) => {
            alert(JSON.stringify(error));
          });
      };
    }

    updateUploadProfilePicture(data) {
        return data;
    }

    uploadProfilePicture(image) {
        let self = this,
            data = new FormData();

        return (dispatch) => {
            dispatch();
            data.append('uploaded_file', { uri: image, name: 'profilePicture.' + image.split('.').reverse()[0], type: 'image/' + image.split('.').reverse()[0] });
            network.fileUpload(data)
            .then((response) => {
                self.updateUploadProfilePicture(response);
                if(response.original) {
                    self.changeProfilePicture(response.original);
                }
            })
            .catch((error) => {
                self.userError(error)
            });
        };
    }

    changeProfilePicture(url) {
        let self = this;

        return (dispatch) => {
            dispatch();

            network.request('user/image', 'POST', [], { url })
            .then((user) => {
                self.updateGetUserById(user);
            })
            .catch((error) => {
              alert(JSON.stringify(error));
            });
        };
    }

    dispatchUpdateUser(user) {
        return user;
    }

    updateUser(data) {
        let self = this;

        return (dispatch) => {
            dispatch();

            network.request('user/me', 'PUT', [], data)
            .then((user) => {
                AsyncStorage.setItem('user', JSON.stringify(user));
                self.updateGet(user);
            })
            .catch((error) => {
                alert(JSON.stringify(error));
            });
        };
    }

    userError(error) {
        return error;
    }
}
export default alt.createActions(UserActions);
