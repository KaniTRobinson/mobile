import { AsyncStorage } from 'react-native';
import alt from '../alt';
import network from '../lib/networking';
import { Actions } from 'react-native-router-flux';

export class GenreActions {
    updateGenres(genres) {
        return genres;
    }

    get() {
        var self = this,
            data = [];

        return (dispatch) => {
            dispatch();
            AsyncStorage.getItem('genres', (tokErr, genres) => {
                  if(genres) {
                      data = JSON.parse(genres);
                  } else {
                      network.request('genres', 'GET')
                      .then((genresQuery) => {
                          data = genresQuery;
                          AsyncStorage.setItem('genres', JSON.stringify(genresQuery));
                      })
                      .catch((error) => {
                        alert(JSON.stringify(error));
                      });
                  }
                  self.updateGenres(data);
            });
        };
    }
}
export default alt.createActions(GenreActions);
