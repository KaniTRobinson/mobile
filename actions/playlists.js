import { AsyncStorage } from 'react-native';
import alt from '../alt';
import network from '../lib/networking';
import { Actions } from 'react-native-router-flux';

export class PlaylistsActions {

    updateGetGenrePlaylists(genrePlaylists) {
        return genrePlaylists;
    }

    getGenrePlaylists(id) {
        var self = this;

        return (dispatch) => {
            dispatch();
            network.request('playlist/genre/' + id, 'GET')
            .then((genrePlaylists) => {
                self.updateGetGenrePlaylists(genrePlaylists);
            })
            .catch((error) => {
              alert(JSON.stringify(error));
            });
        };
    }

    updatePlaylistById(playlists) {
        return playlists;
    }

    getPlaylistsById(id) {
        var self = this;

        return (dispatch) => {
            dispatch();
            network.request('playlist/user/' + id, 'GET')
            .then((playlists) => {
                self.updatePlaylistById(playlists);
            })
            .catch((error) => {
              alert(JSON.stringify(error));
            });
        };
    }

    updateLikePlaylist(booleanResponse) {
        return booleanResponse;
    }

    likePlaylist(likeObject) {
        var self = this;

        return (dispatch) => {
            dispatch();
            if(!likeObject.hasLiked && !likeObject.like_id) {
                network.request('like', 'POST', {
                    'type': 'playlists',
                    'object_id': likeObject['id']
                })
                .then((response) => {
                    self.updateLikePlaylist({
                      'like_id': response['id'],
                      'hasLiked': true
                    });
                })
                .catch((error) => {
                  alert(JSON.stringify(error));
                });
            } else {
                network.request('like/' + likeObject.like_id, 'DELETE')
                .then(() => {
                    self.updateLikePlaylist({
                      'like_id': null,
                      'hasLiked': false
                    });
                })
                .catch((error) => {
                  alert(JSON.stringify(error));
                });
            }
        };
    }

    updatePlaylist(playlist) {
        return playlist;
    }

    getPlaylist(id) {
        var self = this;

        return (dispatch) => {
            dispatch();
            network.request('playlist/' + id, 'GET')
            .then((playlist) => {
                self.updatePlaylist(playlist);
            })
            .catch((error) => {
              alert(JSON.stringify(error));
            });
        };
    }


    updatePlaylists(playlists) {
        return playlists;
    }

    getPlaylists(type='default') {
        var self = this,
            queryParams = [],
            urlAttachment = '';

        queryParams['type'] = type;

        return (dispatch) => {
            dispatch();
            network.request('playlist' + urlAttachment, 'GET', queryParams)
            .then((playlists) => {
                self.updatePlaylists(playlists);
            })
            .catch((error) => {
              alert(JSON.stringify(error));
            });
        };
    }
}
export default alt.createActions(PlaylistsActions);
