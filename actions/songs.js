import { AsyncStorage } from 'react-native';
import alt from '../alt';
import network from '../lib/networking';
import { Actions } from 'react-native-router-flux';

export class SongsActions {
    updateSongs(playlist) {
        return playlist;
    }

    getSongs() {
        var self = this;

        return (dispatch) => {
            dispatch();
            self.updateSongs([]);
        };
    }
}
export default alt.createActions(SongsActions);
