import React from 'react';
import { TouchableHighlight, View, Text, Image, ListView } from 'react-native';
import { Actions } from 'react-native-router-flux';
import CachedImage from 'react-native-cached-image';
// import PlaylistsActions from '../../actions/playlists';
// import PlaylistsStore from '../../stores/playlists';

import application from '../../styles/application';

export default class extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.state.isOpen = false;
        this.state.maxHeight = 55;
        // this.state = PlaylistsStore.getState();
        // this.onChange = this.onChange.bind(this);
    }

    componentWillMount() {
        // PlaylistsStore.listen(this.onChange);
        // PlaylistsActions.getPlaylists(this.props.type);
    }

    componentWillUnmount() {
        // PlaylistsStore.unlisten(this.onChange);
    }

    onChange(state) {
        this.setState({  });
    }

    togglePlayer() {
        this.setState({
          isOpen: (this.state.isOpen) ? false : true
        });
    }

    render() {
        return (
            <View style={[ application.musicPlayerContainer, { height: this.state.height } ]}>
              <Text onPress={ this.togglePlayer.bind(this) } style={ application.text }>{(this.state.isOpen) ? 'Close' : 'Open'}</Text>
            </View>
        )
    }
}
