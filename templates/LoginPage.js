import React from 'react';
import { ScrollView, TextInput, Text, Image, TouchableHighlight } from 'react-native';
import { Actions } from 'react-native-router-flux';

import UserActions from '../actions/user';
import UserStore from '../stores/user';

import application from '../styles/application';

import auth from '../lib/auth';

export default class extends React.Component {
    constructor(props) {
        super(props);
        this.state = UserStore.getState();
        this.state.email = '';
        this.state.password = '';
        this.onChange = this.onChange.bind(this);
    }

    componentWillMount() {
        UserStore.listen(this.onChange);
    }

    componentWillUnmount() {
        UserStore.unlisten(this.onChange);
    }

    onChange(state) {
        this.setState({
            user: state.user,
            token: state.token,
            email: state.email,
            password: state.password
        });
    }

    onLoginPress() {
        var credentials = {
          'email': this.state.email,
          'password': this.state.password
        };
        UserActions.login(credentials);
    }

    render() {
        auth.checkAuth(this.props);
        return (
            <ScrollView style={ application.container }>
                <Image style={[ application.pageLogo, application.pushTop, application.alignCenter ]} source={require('../images/logo-small.png')} />
                <TextInput
                  style={[ application.input, application.pushTop ]}
                  autoCapitalize='none'
                  autoCorrect={ false }
                  autoFocus={ true }
                  keyboardType='email-address'
                  placeholder='email@address.com'
                  value={ this.state.email }
                  onChangeText={ (email) => this.setState({ email }) } />

                <TextInput
                  style={ application.input }
                  autoCapitalize='none'
                  autoCorrect={ false }
                  placeholder='password'
                  secureTextEntry
                  value={ this.state.password }
                  onChangeText={ (password) => this.setState({password}) } />

                <TouchableHighlight onPress={ this.onLoginPress.bind(this) } style={[ application.button, application.buttonFill, application.center90 ]}>
                    <Text style={ application.buttonText }>Log in</Text>
                </TouchableHighlight>
                <Text onPress={ () => Actions.register() } style={[ application.text, application.centerText, application.pushTop ]}>Dont got an account? Register Now!</Text>
            </ScrollView>
        )
    }
}
