import React from 'react';
import { View } from 'react-native';

import application from '../styles/application';

import PlaylistComponent from './components/PlaylistComponent';
import MusicPlayerComponent from './components/MusicPlayerComponent';

export default class extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={ application.container }>
                <PlaylistComponent type='featured' />
                <MusicPlayerComponent />
            </View>
        )
    }
}
