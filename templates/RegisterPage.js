import React from 'react';
import { ScrollView, TextInput, Text, Image, TouchableHighlight } from 'react-native';
import { Actions } from 'react-native-router-flux';

import UserActions from '../actions/user';
import UserStore from '../stores/user';

import application from '../styles/application';

import auth from '../lib/auth';

export default class extends React.Component {
    constructor(props) {
        super(props);
        this.state = UserStore.getState();
        this.state.username = '';
        this.state.first_name = '';
        this.state.last_name = '';
        this.state.email = '';
        this.state.password = '';
        this.state.password_confirmation = '';
        this.onChange = this.onChange.bind(this);
    }

    componentWillMount() {
        UserStore.listen(this.onChange);
    }

    componentWillUnmount() {
        UserStore.unlisten(this.onChange);
    }

    onChange(state) {
        this.setState({
            user: state.user,
            token: state.token,
            username: state.username,
            first_name: state.first_name,
            last_name: state.last_name,
            email: state.email,
            password: state.password,
            password_confirmation: state.password_confirmation
        });
    }

    onRegisterPress() {
        var details = {
          'username': this.state.username,
          'first_name': this.state.first_name,
          'last_name': this.state.last_name,
          'email': this.state.email,
          'password': this.state.password,
          'password_confirmation': this.state.password_confirmation
        };

        UserActions.register(details);
    }

    render() {
        auth.checkAuth(this.props);
        return (
            <ScrollView style={ application.container }>
                <Image style={[ application.pageLogo, application.pushTop, application.alignCenter ]} source={require('../images/logo-small.png')} />
                <TextInput
                  style={[ application.input, application.pushTop ]}
                  autoCapitalize='none'
                  autoCorrect={ false }
                  autoFocus={ true }
                  placeholder='username'
                  value={ this.state.username }
                  onChangeText={ (username) => this.setState({username}) } />

                <TextInput
                  style={ application.input }
                  autoCorrect={ false }
                  placeholder='first name'
                  value={ this.state.first_name }
                  onChangeText={ (first_name) => this.setState({first_name}) } />

                <TextInput
                  style={ application.input }
                  autoCorrect={ false }
                  placeholder='last name'
                  value={ this.state.last_name }
                  onChangeText={ (last_name) => this.setState({last_name}) } />

                <TextInput
                  style={ application.input }
                  autoCapitalize='none'
                  autoCorrect={ false }
                  keyboardType='email-address'
                  placeholder='email@address.com'
                  value={ this.state.email }
                  onChangeText={ (email) => this.setState({email}) } />

                <TextInput
                  style={ application.input }
                  autoCapitalize='none'
                  autoCorrect={ false }
                  placeholder='password'
                  secureTextEntry
                  value={ this.state.password }
                  onChangeText={ (password) => this.setState({password}) } />

                <TextInput
                  style={ application.input }
                  autoCapitalize='none'
                  autoCorrect={ false }
                  placeholder='password'
                  secureTextEntry
                  value={ this.state.password_confirmation }
                  onChangeText={ (password_confirmation) => this.setState({password_confirmation}) } />

                <TouchableHighlight onPress={ this.onRegisterPress.bind(this) } style={[ application.button, application.buttonFill, application.center90 ]} >
                    <Text style={ application.buttonText }>Register</Text>
                </TouchableHighlight>
            </ScrollView>
        )
    }
}
