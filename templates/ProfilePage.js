import React from 'react';
import { View, Text, Image, ListView, TouchableHighlight, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import CachedImage from 'react-native-cached-image';
import Icon from 'react-native-vector-icons/FontAwesome';
import ImagePicker from 'react-native-image-picker';

import UserActions from '../actions/user';
import UserStore from '../stores/user';

import PlaylistsActions from '../actions/playlists';
import PlaylistsStore from '../stores/playlists';

import application from '../styles/application';

import auth from '../lib/auth';

export default class extends React.Component {
    constructor(props) {
        super(props);
        this.state = Object.assign(UserStore.getState(), PlaylistsStore.getState());
        this.state.isMe = false;
        this.state.user_id = (this.props.user_id) ? this.props.user_id : 1;
        this.state.count = 0;
        this.state.followers = 0;
        this.state.following = 0;
        this.state.dataSource = new ListView.DataSource({
            rowHasChanged: (r1, r2) =>  r1 !== r2
        });
        this.onChange = this.onChange.bind(this);
        this.onPlaylistsChange = this.onPlaylistsChange.bind(this);
    }

    componentWillMount() {
        PlaylistsStore.listen(this.onPlaylistsChange);
        UserStore.listen(this.onChange);
        UserActions.getMe();
        if(this.state.user_id) {
            UserActions.getUserById(this.state.user_id);
        }
        if(this.state.user_id) {
            PlaylistsActions.getPlaylistsById(this.state.user_id);
        }
    }

    componentWillUnmount() {
        PlaylistsStore.unlisten(this.onPlaylistsChange);
        UserStore.unlisten(this.onChange);
    }

    onChange(state) {
        this.setState({
            me: state.me,
            user: state.user,
            isMe: ((state.me && state.user) && (state.me.id && state.user.id) && (state.me.id === state.user.id)) ? true : false
        });
    }

    onPlaylistsChange(state) {
        this.setState({
            count: state.userPlaylists.length,
            userPlaylists: state.userPlaylists,
            dataSource: this.state.dataSource.cloneWithRows(state.userPlaylists)
        });
    }

    selectPhotoTapped() {
        if(this.state.isMe) {
            ImagePicker.showImagePicker({
                quality: 1.0,
                maxWidth: 500,
                maxHeight: 500,
                storageOptions: {
                    skipBackup: true
                }
            }, (response) => {
              if (!response.didCancel && !response.error) {
                  UserActions.uploadProfilePicture(response.uri);
              }
            });
        }
    }

    _renderHeader() {
        return(
            <View style={[ application.header, application.headerPush ]}>
                <View style={[ application.HeaderImageContainer, application.HeaderImageContainerBlend ]}>
                    <View style={{ right: 20, top: 20, position: 'absolute' }}>
                      {this.state.isMe === true &&
                        <TouchableHighlight onPress={ () => alert('Create Playlist') } >
                            <Icon color="#FFF" size={ 16 } name="plus" />
                        </TouchableHighlight>
                      }
                    </View>
                    <TouchableOpacity onPress={this.selectPhotoTapped.bind(this)}>
                        <CachedImage source={{ url: this.state.user.image }} style={ application.headerAvatar } />
                    </TouchableOpacity>
                    <Text onPress={ () => this.launchImagePicker.bind(this) } style={ application.headerText }>{ this.state.user.first_name } { this.state.user.last_name }</Text>
                    <Text style={ application.headerAuthor }>{ this.state.user.username }</Text>
                    {this.state.isMe === true &&
                        <View style={ application.flexRow }>
                            <TouchableHighlight onPress={ () => Actions.settings() } style={[ application.button, application.pushRight ]}>
                                <Text style={ application.buttonSmallText }>SETTINGS</Text>
                            </TouchableHighlight>
                            <TouchableHighlight onPress={ () => UserActions.logout() } style={[ application.button, application.buttonRed ]}>
                                <Icon color="#FFF" size={ 14 } name="sign-out" />
                            </TouchableHighlight>
                        </View>
                    }
                    {this.state.isMe === false &&
                        <View style={ application.flexRow }>
                            <TouchableHighlight onPress={ () => alert('FOLLOW') } style={[ application.button, application.pushRight ]}>
                                <Text style={ application.buttonSmallText }>FOLLOW</Text>
                            </TouchableHighlight>
                            <TouchableHighlight onPress={ () => alert('REPORT') } style={[ application.button, application.buttonRed ]}>
                                <Text style={ application.buttonSmallText }>REPORT</Text>
                            </TouchableHighlight>
                        </View>
                    }
                </View>
                <View style={[ application.flexRow, application.statsBar ]}>
                    <View style={[ application.flex, application.alignChildren ]}>
                        <Text style={[ application.lightText, application.statsText2 ]}>{ this.state.followers }</Text>
                        <Text style={ application.text }>Followers</Text>
                    </View>
                    <View style={[ application.flex, application.alignChildren ]}>
                        <Text style={[ application.lightText, application.statsText2 ]}>{ this.state.following }</Text>
                        <Text style={ application.text }>Following</Text>
                    </View>
                    <View style={[ application.flex, application.alignChildren ]}>
                        <Text style={[ application.lightText, application.statsText ]}>{ this.state.count }</Text>
                        <Text style={ application.lightText }>Playlists</Text>
                    </View>
                </View>
                <View style={ application.title }>
                  <Text style={ application.titleText }>{ this.state.user.first_name } { this.state.user.last_name } Playlists</Text>
                </View>
            </View>
        )
    }

    _renderRow(data, section, id, highlightRow) {
      return (
          <View style={ application.item }>
            <TouchableHighlight activeOpacity={ 0.6 } onPress={ () => Actions.myPlaylist({ title: data.name.toUpperCase(), data: data}) }>
              <CachedImage style={ application.playListImage } resizeMode='contain' source={{ uri: data.image }} />
            </TouchableHighlight>
            <Text style={ application.playListTitle }>{ data.name }</Text>
            <Text style={ application.playListAuthor }>{ data.user.first_name } { data.user.last_name }</Text>
          </View>
      );
    }

    render() {
        auth.checkAuth(this.props);
        return (
            <View style={ application.container }>
                <ListView
                    dataSource={ this.state.dataSource }
                    renderHeader={ this._renderHeader.bind(this) }
                    renderRow={ this._renderRow.bind(this) }
                    enableEmptySections={ true }
                    contentContainerStyle={[ application.list, application.removeListMargin ]}
                    automaticallyAdjustContentInsets={ false }
                    renderEmptyListComponent={ () => (<Text>Empty</Text>) }
                />
            </View>
        )
    }
}
