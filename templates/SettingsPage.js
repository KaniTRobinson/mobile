import React from 'react';
import { View, Text, TextInput, Image, ScrollView, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import CachedImage from 'react-native-cached-image';
import ImagePicker from 'react-native-image-picker';
import ScrollableTabView, { DefaultTabBar } from 'react-native-scrollable-tab-view';

import UserActions from '../actions/user';
import UserStore from '../stores/user';

import application from '../styles/application';

import auth from '../lib/auth';

export default class extends React.Component {
    constructor(props) {
        super(props);
        this.state = UserStore.getState();
        this.onChange = this.onChange.bind(this);
        this.state.first_name = this.state.me.first_name;
        this.state.last_name = this.state.me.last_name;
        this.state.password = '';
        this.state.password_confirmation = '';
    }

    componentWillMount() {
        UserStore.listen(this.onChange);
    }

    componentWillUnmount() {
        UserStore.unlisten(this.onChange);
    }

    onChange(state) {
        this.setState({
            me: state.me
        });
    }

    updateGeneralInformation() {
        UserActions.updateUser({
           first_name: this.state.first_name,
           last_name: this.state.last_name
        });
    }

    render() {
        auth.checkAuth(this.props);
        return (
            <View style={ application.container }>
                {this.state.me &&
                  <ScrollableTabView tabBarBackgroundColor='#161616' tabBarTextStyle={ application.text } tabBarUnderlineStyle={{ opacity: 0 }} renderTabBar={ () => <DefaultTabBar /> } ref={ (tabView) => { this.tabView = tabView; } }>
                        <ScrollView tabLabel='General Settings'>
                            <View style={ application.title }>
                              <Text style={ application.titleText }>General Settings</Text>
                            </View>
                            <TextInput
                              style={[ application.input, application.pushTop, application.inputTextSmaller ]}
                              autoCorrect={ false }
                              placeholder='first name'
                              value={ this.state.first_name }
                              onChangeText={ (first_name) => this.setState({first_name}) } />
                            <TextInput
                              style={[ application.input, application.inputTextSmaller ]}
                              autoCorrect={ false }
                              placeholder='last name'
                              value={ this.state.last_name }
                              onChangeText={ (last_name) => this.setState({last_name}) } />
                            <TouchableOpacity onPress={ this.updateGeneralInformation.bind(this) } style={[ application.button, application.buttonFill, application.center90 ]} >
                                <Text style={ application.buttonText }>Save</Text>
                            </TouchableOpacity>
                        </ScrollView>
                        <ScrollView tabLabel='Change Password'>
                            <View style={ application.title }>
                              <Text style={ application.titleText }>Change Password</Text>
                            </View>
                            <TextInput
                              style={[ application.input, application.pushTop, application.inputTextSmaller ]}
                              autoCapitalize='none'
                              autoCorrect={ false }
                              placeholder='password'
                              secureTextEntry
                              value={ this.state.password }
                              onChangeText={ (password) => this.setState({password}) } />

                            <TextInput
                              style={[ application.input, application.inputTextSmaller ]}
                              autoCapitalize='none'
                              autoCorrect={ false }
                              placeholder='confirm password'
                              secureTextEntry
                              value={ this.state.password_confirmation }
                              onChangeText={ (password_confirmation) => this.setState({password_confirmation}) } />

                              <TouchableOpacity onPress={ () => alert('Update Password') } style={[ application.button, application.buttonFill, application.center90 ]} >
                                  <Text style={ application.buttonText }>Update Password</Text>
                              </TouchableOpacity>
                        </ScrollView>
                    </ScrollableTabView>
                }
            </View>
        )
    }
}
