import React from 'react';
import { View, Text, Image, TouchableHighlight, ListView } from 'react-native';
import { Actions } from 'react-native-router-flux';
import CachedImage from 'react-native-cached-image';
import GenreActions from '../actions/genre';
import GenreStore from '../stores/genre';

import application from '../styles/application';

export default class extends React.Component {
    constructor(props) {
        super(props);
        this.state = GenreStore.getState();
        this.state.dataSource = new ListView.DataSource({
            rowHasChanged: (r1, r2) =>  r1 !== r2
        });
        this.onChange = this.onChange.bind(this);
    }

    componentWillMount() {
        GenreStore.listen(this.onChange);
        GenreActions.get();
    }

    componentWillUnmount() {
        GenreStore.unlisten(this.onChange);
    }

    onChange(state) {
        this.setState({
            genres: state.genres,
            dataSource: this.state.dataSource.cloneWithRows(state.genres)
        });
    }

    _renderRow(data, section, id, highlightRow) {
        return (
            <TouchableHighlight style={ application.genreContainer } activeOpacity={ 0.6 } onPress={ () => Actions.genrePlaylists({ data: data, title: data.name.toUpperCase() + ' PLAYLISTS' }) }>
                <CachedImage style={ application.genreImage } blurRadius={ 6 } source={{ uri: data.image }}>
                    <Text style={ application.genreText }>{ data.name }</Text>
                </CachedImage>
            </TouchableHighlight>
        );
    }

    render() {
        return (
            <View style={ application.container }>
                <ListView
                    dataSource={ this.state.dataSource }
                    renderRow={ this._renderRow.bind(this) }
                    enableEmptySections={ true }
                    contentContainerStyle={[ application.list, application.removeListMargin ]}
                    automaticallyAdjustContentInsets={ false }
                />
            </View>
        )
    }
}
