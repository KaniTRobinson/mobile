import React from 'react';
import { View, Text, Image, ScrollView } from 'react-native';
import { Actions } from 'react-native-router-flux';

import application from '../styles/application';

export default class extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={ application.container }>
              <Text style={ application.text }>Search</Text>
            </View>
        )
    }
}
