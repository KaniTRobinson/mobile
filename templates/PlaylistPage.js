import React from 'react';
import { AsyncStorage, View, Text, Image, TouchableHighlight, ListView } from 'react-native';
import { Actions } from 'react-native-router-flux';
import CachedImage from 'react-native-cached-image';
import PlaylistsActions from '../actions/playlists';
import PlaylistsStore from '../stores/playlists';
import SongsActions from '../actions/songs';
import SongsStore from '../stores/songs';

import application from '../styles/application';

export default class extends React.Component {
    constructor(props) {
        super(props);
        this.state = Object.assign(SongsStore.getState(), PlaylistsStore.getState());
        this.state.isLoggedIn = false;
        this.state.dataSource = new ListView.DataSource({
            rowHasChanged: (r1, r2) =>  r1 !== r2
        });
        this.onSongsChange = this.onSongsChange.bind(this);
        this.onPlaylistsChange = this.onPlaylistsChange.bind(this);
    }

    componentWillMount() {
        SongsStore.listen(this.onSongsChange);
        PlaylistsStore.listen(this.onPlaylistsChange);
        SongsActions.getSongs();
        PlaylistsActions.getPlaylist(this.props.data.id);
        this.checkUser(this);
    }

    componentWillUnmount() {
        SongsStore.unlisten(this.onSongsChange);
        PlaylistsStore.unlisten(this.onPlaylistsChange);
    }

    checkUser(self) {
      AsyncStorage.getItem('token', (tokErr, token) => {
          self.setState({
              isLoggedIn: (token) ? true: false
          });
      });
    }

    onSongsChange(state) {
        this.setState({
            songs: state.songs,
            isFollowing: state.isFollowing,
            dataSource: this.state.dataSource.cloneWithRows(state.songs)
        });
    }

    onPlaylistsChange(state) {
        this.setState({
            playlist: state.playlist
        });
    }

    _toggleFollow() {
        // Do Nothing
    }

    _toggleLike() {
        PlaylistsActions.likePlaylist({
          'id': this.props.data.id,
          'hasLiked': this.state.playlist.hasLiked,
          'like_id': this.state.playlist.like_id
        });
    }

    _renderHeader() {
        return(
            <View style={ application.header }>
              <View style={[ application.HeaderImageContainer, application.HeaderImageContainerBlend ]}>
                <CachedImage style={ application.playListHeaderImage } source={{ uri: this.state.playlist.image }} />
                <Text style={ application.headerText }>{ this.state.playlist.name }</Text>
                <Text style={ application.headerAuthor }>{ (this.state.playlist.user) ? this.state.playlist.user.first_name : this.props.data.user.first_name } { (this.state.playlist.user) ? this.state.playlist.user.last_name : this.props.data.user.last_name }</Text>
                {this.state.isLoggedIn === true &&
                  <View style={ application.flexRow }>
                    <TouchableHighlight onPress={ this._toggleFollow.bind(this) } style={[ application.button, application.pushRight, this.state.isFollowing && application.buttonActive]}>
                        <Text style={ application.buttonSmallText }>{ this.state.isFollowing ? 'FOLLOWING':'FOLLOW' }</Text>
                    </TouchableHighlight>
                    <TouchableHighlight onPress={ this._toggleLike.bind(this) } style={[ application.button, this.state.playlist.hasLiked && application.buttonActive ]}>
                        <Text style={ application.buttonSmallText }>{ this.state.playlist.hasLiked ? 'UNLIKE':'LIKE' }</Text>
                    </TouchableHighlight>
                  </View>
                }
              </View>
            </View>
        )
    }

    _renderRow(data, section, id, highlightRow) {
        return (
            <View style={ application.songItem }>
                <View style={ application.alignContents }>
                    <View style={ application.songIDContainer }>
                        <Text style={[ application.lightText, application.songID ]}>{ id }</Text>
                    </View>
                    <View style={ application.song }>
                      <Text style={[ application.lightText, application.songName ]}>{ this.state.playlist.title }</Text>
                      <Text style={[ application.lightText, application.songAuthor ]}>{ this.state.playlist.user.first_name } { this.state.playlist.user.last_name }</Text>
                    </View>
                </View>
            </View>
        );
    }

    render() {
        return (
            <View style={ application.container }>
                <ListView
                    dataSource={ this.state.dataSource }
                    renderHeader={ this._renderHeader.bind(this) }
                    renderRow={ this._renderRow.bind(this) }
                    enableEmptySections={ true }
                    contentContainerStyle={[ application.list, application.removeListMargin ]}
                    automaticallyAdjustContentInsets={ false }
                    renderEmptyListComponent={ () => (<Text>Empty</Text>) }
                />
            </View>
        )
    }
}
