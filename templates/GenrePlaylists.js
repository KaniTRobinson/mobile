import React from 'react';
import { TouchableHighlight, View, Text, Image, ListView } from 'react-native';
import { Actions } from 'react-native-router-flux';
import CachedImage from 'react-native-cached-image';
import PlaylistsActions from '../actions/playlists';
import PlaylistsStore from '../stores/playlists';

import application from '../styles/application';

export default class extends React.Component {
    constructor(props) {
        super(props);
        this.state = PlaylistsStore.getState();
        this.state.dataSource = new ListView.DataSource({
            rowHasChanged: (r1, r2) =>  r1 !== r2
        });
        this.onChange = this.onChange.bind(this);
    }

    componentWillMount() {
        PlaylistsStore.listen(this.onChange);
        PlaylistsActions.getGenrePlaylists(this.props.data.id);
    }

    componentWillUnmount() {
        PlaylistsStore.unlisten(this.onChange);
    }

    onChange(state) {
        this.setState({
            genrePlaylists: state.genrePlaylists,
            dataSource: this.state.dataSource.cloneWithRows(state.genrePlaylists)
        });
    }

    _renderHeader() {
        return(
            <View style={ application.title }>
              <Text style={ application.titleText }>{ this.props.data.name } Playlists</Text>
            </View>
        )
    }

    _renderRow(data, section, id, highlightRow) {
        return (
            <View style={ application.item }>
              <TouchableHighlight activeOpacity={ 0.6 } onPress={ () => Actions.genrePlaylist({ title: data.name.toUpperCase(), data: data}) }>
                <CachedImage style={ application.playListImage } resizeMode='contain' source={{ uri: data.image }} />
              </TouchableHighlight>
              <Text style={ application.playListTitle }>{ data.name }</Text>
              <Text onPress={ () => Actions.profile({ user_id: data.user.id }) }  style={ application.playListAuthor }>{ data.user.first_name } { data.user.last_name }</Text>
            </View>
        );
    }

    render() {
        return (
            <View style={ application.container }>
                <ListView
                    dataSource={ this.state.dataSource }
                    renderHeader={ this._renderHeader.bind(this) }
                    renderRow={ this._renderRow.bind(this) }
                    enableEmptySections={ true }
                    contentContainerStyle={ application.list }
                    automaticallyAdjustContentInsets={ false }
                />
            </View>
        )
    }
}
