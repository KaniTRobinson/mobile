import { Platform } from 'react-native';

var env = {
    isDevelopment: __DEV__,
    currentEnvironment: (__DEV__) ? 'development' : 'production',
    namespace: '/api/',
    cdnDomain: 'https://cdn.datmusic.co.uk/create/image',
    environments: {
      production: {
          title: 'Production',
          domain: 'https://api.datmusic.co.uk'
      },
      development: {
          title: 'Development',
          domain: 'http://sandbox.datmusic.co.uk:8000'
      }
    }
};

for(var current in env.environments[env.currentEnvironment]) {
    env[current] = env.environments[env.currentEnvironment][current];
}

delete env.environments;

if (env.currentEnvironment === 'Development') {}

if (env.currentEnvironment === 'Production') {}

module.exports = env;
