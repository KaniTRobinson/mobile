import { Actions } from 'react-native-router-flux';

export default class auth {

    static checkAuth(props) {
        if(props.nonAuthOnly && props.user !== null && props.token !== '') {
            Actions.profile();
        }

        if(props.authOnly && props.user === null && props.token === '') {
            Actions.login();
        }
    }
}
