import { AsyncStorage, NetInfo } from 'react-native';
import env from '../env';

export default class network {

    static buildURL(resourceName, queryParams=[]) {
        var Params = '';

        if(queryParams) {
            Params = Params + '?' + Object.keys(queryParams)
            .map(key => key + '=' + encodeURIComponent(queryParams[key]))
            .join('&');
        }

        if (Params.substring(Params.length-1) == '&') {
            Params = Params.substring(0, Params.length-1);
        }

        // TODO: Environment File with URL path and API namespace
        return  env.domain + env.namespace + resourceName + Params;
    }

    static fileUpload(data) {
        let self = this;

        return new Promise((resolve, reject) => {
            return fetch(env.cdnDomain, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'multipart/form-data; boundary=6ff46e0b6b5148d984f148b6542e5a5d',
                    'Content-Language': 'en'
                },
                body: data,
            })
            .then(response => response.json()
            .then(photo => ({ photo, response })))
            .then(({ photo, response }) => {
                if(!response.ok) {
                    reject(photo);
                } else {
                    resolve(photo);
                }
            }).catch(err => {
                reject(err);
            });
        });
    }

    static request(resourceName, method, queryParams=[], body=null) {
        var self = this,
        options = {};

        return new Promise((resolve, reject) => {
          AsyncStorage.getItem('token', (tokErr, token) => {
                options['method'] = method;
                options['headers'] = {};
                options['headers']['Accept'] = 'application/json';
                options['headers']['Content-Type'] = 'application/json';

                if(token) {
                    queryParams['token'] = token;
                }

                if(body !== null) {
                    options['body'] = JSON.stringify(body);
                }

                fetch(self.buildURL(resourceName, queryParams), options)
                .then((response) => {
                    if(response.status === 500) {
                        return response.text()
                        .then((error) => {
                            if (/^[\],:{}\s]*$/.test(error.replace(/\\["\\\/bfnrtu]/g, '@').
                                replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
                                replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {

                                reject(JSON.parse(error));
                            } else {
                                console.log(resourceName);
                                console.log(error);
                                // if(error.includes('')) {
                                //
                                // } else {
                                //     reject({'error': ['Unexpected Error..']});
                                // }
                            }
                        });
                    } else {
                        return response.json();
                    }
                })
                .then((response) => {
                    resolve(response);
                })
                .catch((error) => {
                    console.log(error);
                    reject(error);
                });
            });
        });
    }
}
